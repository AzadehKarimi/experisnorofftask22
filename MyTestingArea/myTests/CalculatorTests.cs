﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using ClassForTesting;

namespace myTests
{
    public class CalculatorTests
    {
        
        //[Fact]
        [Theory]
        [InlineData(6, 4, 10)]
        [InlineData(56.3, 2.1, 58.4)]
        public void Add_WhatIsBeingTeste(double num1, double num2, double expected)
        {
            //Arrange
             //double expected = 10;
            //Act
            double actual = Calculator.Add(num1, num2);
            //Assert
            Assert.Equal(expected, actual);

        }
        //[Fact]
        [Theory]
        [InlineData(6, 4, 2)]
        [InlineData(6, 2, 4)]
        void Subtract_WhatIsBeingTeste(double num1, double num2, double expected)
        {
            //Arrange
            // double expected = 10;
            //Act
            //double actual = Calculator.Subtract(num1, num2);
            //Assert
            //Assert.Equal(expected, actual);

        }
        //[Fact]
        [Theory]
        [InlineData(6, 4, 24)]
        [InlineData(5, 6, 30)]
        public void Multiply_WhatIsBeingTeste(double num1, double num2, double expected)
        {

            //Arrange
            // double expected = 10;
            //Act
            //double actual = Calculator.Multiply(num1, num2);
            //Assert
            // Assert.Equal(expected, actual);
        }
        //[Fact]
        [Theory]
        [InlineData(15, 43, 5)]
        [InlineData(10, 5, 2)]
        public void Divid_WhatIsBeingTeste(double num1, double num2, double expecte)
        {
            //Arrange
            // double expected = 10;
            //Act
            //double actual = Calculator.Divid(num1, num2);
            //Assert
            // Assert.Equal(expected, actual);
        }
        
        //********************************************************************'
        // This method is checking zero diviation  


        //[Fact] 
        [Theory]
        [InlineData(8, 0,false )]
        [InlineData(6,2,true)]
        public void DividByZero(double num1, double num2, bool expected)
        {
            
            Exception ex = Record.Exception(() => Calculator.Divide(num1, num2));
            if (!expected)
            {
                Assert.NotNull(ex);
                Assert.IsType<Exception>(ex);
            }
            else
            {
                Assert.Null(ex);
            }

        }
        //********************************************************************
        //This method checking the result of subtract be posetive.
        [Theory]
        [InlineData(5, 7, false)]
        [InlineData(6, 2, true)]
        public void NegativeSubtract(double num1, double num2, bool expected)
        {

            Exception ex = Record.Exception(() => Calculator.Subtract(num1, num2));
            if (!expected)
            {
                Assert.NotNull(ex);
                Assert.IsType<Exception>(ex);
            }
            else
            {
                Assert.Null(ex);
            }

        }

    }
}
